import React from 'react';

export function Column({ width = 1, text }) {
  return <div style={{ flex: width }}>{text}</div>;
}

const validateColumns = columns => {
  if (!Array.isArray(columns)) {
    console.log('Columns must be an array');
    return false;
  }
  if (columns.some(({ width }) => width < 1 || width > 12)) {
    console.log('At least one column has invalid width');
    return false;
  }
  const totalWidth = columns.reduce((acc, { width }) => acc + width, 0);
  if (totalWidth !== 12) {
    console.log('Column widths must add up to 12');
    return false;
  }
  return true;
};

export default function Grid({ columns }) {
  if (!validateColumns(columns)) {
    return null;
  }

  return (
    <div style={{ display: 'flex' }}>
      {columns.map((columnData, key) => (
        <Column key={key} {...columnData} />
      ))}
    </div>
  );
}
