import './App.css';
import Grid from './components/Grid';

const gridAColumns = [
  {
    width: 4,
  },
  {
    width: 6,
    text: 'Lorem ipsum dolor sit amet',
  },
  {
    width: 2,
  },
];

const gridBColumns = [
  {
    width: 6,
    text: 'Lorem ipsum dolor sit amet',
  },
  {
    width: 6,
    text: 'Lorem ipsum dolor sit amet',
  },
];

const GridA = () => <Grid columns={gridAColumns} />
const GridB = () => <Grid columns={gridBColumns} />

function App() {
  return (
    <div className="App">
      <GridA />
      <GridB />
    </div>
  );
}

export default App;
